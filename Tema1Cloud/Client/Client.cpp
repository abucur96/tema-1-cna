#include <iostream>
#include <SumOperation.grpc.pb.h>
#include <DisplayNameOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
	grpc_init();
	ClientContext context;
	auto sum_stub = DisplayNameOperationService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	DisplayNameRequest nameRequest;
	std::cout << "Insert your name here:";

	std::string name;

	std::cin >> name;

	nameRequest.set_clientname(name);
	Empty response;
	auto status = sum_stub->DisplayName(&context, nameRequest, &response);
}