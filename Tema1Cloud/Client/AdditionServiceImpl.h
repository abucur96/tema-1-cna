#pragma once

#include"SumOperation.grpc.pb.h"
#include "DisplayNameOperation.grpc.pb.h"

class AdditionServiceImpl final : public DisplayNameOperationService::Service
{
public:
	AdditionServiceImpl() {};
	::grpc::Status DisplayName(::grpc::ServerContext* context, const ::DisplayNameRequest* request, ::Empty* response) override;
};