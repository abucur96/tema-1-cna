#include "AdditionServiceImpl.h"

::grpc::Status AdditionServiceImpl::DisplayName(::grpc::ServerContext* context, const::DisplayNameRequest* request, ::Empty* response)
{
	std::string clientName = request->clientname();
	std::cout << "Hello " << clientName << "\n";
	return ::grpc::Status::OK;
}